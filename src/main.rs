use std::convert::TryFrom;
use std::env::args_os;
use std::fs::read_to_string;

use chrono::{offset::Utc, Datelike, Duration, TimeZone};
use geo::{algorithm::contains::Contains, Coordinate, Geometry};
use geojson::GeoJson;
use ndarray::{Array2, ArrayBase, Axis, Zip};
use netcdf::open;
use proj::Proj;

fn main() {
    let mut args = args_os();
    let path = args.nth(1).unwrap();
    let countries_path = args.next().unwrap();
    let country_id = args.next().unwrap().into_string().unwrap();

    let countries = read_to_string(countries_path)
        .unwrap()
        .parse::<GeoJson>()
        .unwrap();

    let geometry = match countries {
        GeoJson::FeatureCollection(collection) => {
            let feature = collection
                .features
                .into_iter()
                .find(|feat| {
                    feat.properties
                        .as_ref()
                        .and_then(|props| props.get("CNTR_ID"))
                        .map_or(false, |prop| prop == &country_id)
                })
                .unwrap();

            Geometry::<f64>::try_from(feature.geometry.unwrap()).unwrap()
        }
        _ => unreachable!(),
    };

    let file = open(&path).unwrap();

    let lat = file.variable("lat").unwrap();
    let lat = lat.values::<f64>(None, None).unwrap();

    let lon = file.variable("lon").unwrap();
    let lon = lon.values::<f64>(None, None).unwrap();

    assert_eq!(lat.shape(), lon.shape());
    let shape = match lat.shape() {
        [width, height] => [*width, *height],
        _ => unreachable!(),
    };
    let lat = ArrayBase::into_shape(lat, shape).unwrap();
    let lon = ArrayBase::into_shape(lon, shape).unwrap();

    let mut mask = Array2::<f32>::from_elem(shape, 0.0);

    let proj = Proj::new_known_crs("EPSG:4326", "EPSG:3035", None).unwrap();

    Zip::from(&mut mask)
        .and(&lat)
        .and(&lon)
        .apply(|mask, lat, lon| {
            let coord = proj.convert(Coordinate { y: *lat, x: *lon }).unwrap();

            if geometry.contains(&coord) {
                *mask = 1.0;
            }
        });

    let mask = mask.view();
    let cells = mask.sum();

    let time = file.variable("time").unwrap();
    let time = time.values::<f64>(None, None).unwrap();

    assert_eq!(time.ndim(), 1);
    let len = time.shape()[0];
    let time = ArrayBase::into_shape(time, len).unwrap();

    let epoch = Utc.ymd(1949, 12, 1);
    let time_to_year = |time| (epoch + Duration::days(time as i64)).year();

    let tas = file.variable("tas").unwrap();

    let print_mean = |year, idx, days| {
        let tas = tas
            .values::<f32>(Some(&[idx, 0, 0]), Some(&[days, shape[0], shape[1]]))
            .unwrap();

        let sum_over_days = tas.sum_axis(Axis(0)) * mask;
        let mean_annual_tas = sum_over_days.sum() / cells / days as f32;

        println!("{}, {}", year, mean_annual_tas);
    };

    println!("year, mean_annual_tas");

    let mut last_year = time_to_year(time[0]);
    let mut first_idx = 0;

    for (idx, time) in time.indexed_iter() {
        let year = time_to_year(*time);

        if last_year == year {
            continue;
        }

        let days = idx - first_idx;

        print_mean(last_year, first_idx, days);

        last_year = year;
        first_idx = idx;
    }

    let days = len - first_idx;

    if days != 0 {
        print_mean(last_year, first_idx, days);
    }
}
